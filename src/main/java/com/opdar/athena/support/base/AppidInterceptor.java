package com.opdar.athena.support.base;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.Interceptor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by shiju on 2017/1/24.
 */
public class AppidInterceptor implements Interceptor,ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public boolean before() {
        if(Context.getRequest().getParameterMap().containsKey("appId")){
            return true;
        }
        String referer = Context.getRequest().getHeader("Referer");
        if(referer != null){
            int index = referer.indexOf("?appId=");
            if(index >= 0){
                String appId = referer.substring(index);
                Context.getRequest().getParameterMap().put("appId",new String[]{appId});
            }
        }
        return false;
    }

    @Override
    public boolean after() {
        return true;
    }

    @Override
    public void finish() {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
