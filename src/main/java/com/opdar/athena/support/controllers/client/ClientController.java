package com.opdar.athena.support.controllers.client;

import com.opdar.athena.support.base.*;
import com.opdar.athena.support.entities.ConversationEntity;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.athena.support.entities.WeightEntity;
import com.opdar.athena.support.service.SupportUserService;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
public class ClientController {

    @Autowired
    SupportUserService supportUserService;
    @Autowired
    ICacheManager<String,String,Object> cacheManager;

    @Interceptor({CommonInterceptor.class})
    @Request(value = "/support/find", format = Request.Format.JSON)
    public Result findSupport(Integer reload){
        UserEntity user = (UserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        if (!cacheManager.exist(appId)) {
            boolean ret = supportUserService.init(appId);
            if (!ret) return Result.valueOf(1,"");
        }
        List<WeightEntity> weights = (List<WeightEntity>) cacheManager.get(appId);
        ConversationEntity conversationEntity = supportUserService.getSupport(user.getId(),user.getUtmSource(), reload, appId, weights);
        if(conversationEntity == null){
            //开启与机器人会话
        }
        return Result.valueOf(conversationEntity);
    }

    @Interceptor(ClientViewInterceptor.class)
    @Request(value = "/client/index", format = Request.Format.VIEW)
    public String index(){
        return "client/index";
    }

}
